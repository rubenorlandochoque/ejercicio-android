package com.rubenorlandochoque.ejercicio.models

data class MainImage(var thumbnailUrl: String, var url: String)