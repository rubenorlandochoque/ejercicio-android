package com.rubenorlandochoque.ejercicio.models

data class Device(
    var mainImage: MainImage,
    var name: String,
    var installmentsTag: String,
    var topTag: String,
    var images: List<MainImage>,
    var legal: String
)