package com.rubenorlandochoque.ejercicio.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenorlandochoque.ejercicio.models.Device
import com.rubenorlandochoque.ejercicio.services.ApiResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DevicesViewModel : ViewModel() {
    private val devices: MutableLiveData<List<Device>> by lazy {
        MutableLiveData<List<Device>>().also {
            loadDevices()
        }
    }

    private val device: MutableLiveData<Device> by lazy {
        MutableLiveData<Device>()
    }

    fun getDevices(): LiveData<List<Device>> {
        return devices
    }

    fun getDevice(id: Int): LiveData<Device> {
        return device.also { loadDevice(id) }
    }

    private fun loadDevices() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = ApiResource.create().getDevices()
            withContext(Dispatchers.Main) {
                devices.value = response.body() ?: emptyList()
            }
        }


//        apiInterface.enqueue(object : Callback<List<Device>> {
//            override fun onResponse(call: Call<List<Device>>?, response: Response<List<Device>>?) {
//
//            }
//
//            override fun onFailure(call: Call<List<Device>>?, t: Throwable?) {
//
//            }
//        })
    }

    private fun loadDevice(id: Int) {
        val apiInterface = ApiResource.create().getDevice(id)
        apiInterface.enqueue(object : Callback<Device> {
            override fun onResponse(call: Call<Device>?, response: Response<Device>?) {
                device.value = response?.body()
            }

            override fun onFailure(call: Call<Device>?, t: Throwable?) {

            }
        })
    }
}