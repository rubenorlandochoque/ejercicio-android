package com.rubenorlandochoque.ejercicio.services

import com.rubenorlandochoque.ejercicio.models.Device
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiResource {

    @GET("devices")
    suspend fun getDevices() : Response<List<Device>>

    @GET("devices/{id}")
    fun getDevice(@Path("id") id: Int) : Call<Device>

    companion object {
        var BASE_URL = "https://61967289af46280017e7e0c0.mockapi.io/"
        fun create() : ApiResource {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiResource::class.java)
        }
    }
}