package com.rubenorlandochoque.ejercicio.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.rubenorlandochoque.ejercicio.R
import com.rubenorlandochoque.ejercicio.databinding.ItemBinding

class ImageViewAdapter(private var fragment: Fragment, private val images: List<String>) :
    PagerAdapter() {
    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, item: Any): Boolean {
        return view === item
    }

    override fun instantiateItem(viewGroup: ViewGroup, position: Int): Any {
        val binding = ItemBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        Glide.with(fragment.requireContext())
            .load(images[position])
            .error(R.drawable.notimage)
            .placeholder(R.drawable.ic_hourglass)
            .fitCenter()
            .into(binding.imageItem)
        viewGroup.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(viewGroup: ViewGroup, position: Int, item: Any) {
        viewGroup.removeView(item as LinearLayout)
    }
}