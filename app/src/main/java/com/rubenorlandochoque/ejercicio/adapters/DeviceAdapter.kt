package com.rubenorlandochoque.ejercicio.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rubenorlandochoque.ejercicio.R
import com.rubenorlandochoque.ejercicio.databinding.DeviceRowBinding
import com.rubenorlandochoque.ejercicio.models.Device

class DeviceAdapter(private val fragment: Fragment, private val devices: List<Device>) :
    RecyclerView.Adapter<DeviceAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = DeviceRowBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(binding, fragment)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val device = devices[position]
        viewHolder.bind(device, position)
    }

    override fun getItemCount(): Int {
        return devices.size
    }

    class ViewHolder(
        private val deviceRowBinding: DeviceRowBinding,
        private val fragment: Fragment
    ) :
        RecyclerView.ViewHolder(deviceRowBinding.root) {
        fun bind(device: Device, position: Int) {
            with(deviceRowBinding) {
                txtName.text = device.name
                txtTopTag.text = device.topTag
                txtInstallmentsTag.text = device.installmentsTag
                Glide.with(fragment.requireContext())
                    .load(device.mainImage.thumbnailUrl)
                    .error(R.drawable.notimage)
                    .placeholder(R.drawable.ic_hourglass)
                    .centerCrop()
                    .into(image)
                contentRow.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putInt("deviceId", position)
                    NavHostFragment.findNavController(fragment)
                        .navigate(R.id.action_DevicesFragment_to_DeviceDetailFragment, bundle)
                }
            }
        }
    }
}