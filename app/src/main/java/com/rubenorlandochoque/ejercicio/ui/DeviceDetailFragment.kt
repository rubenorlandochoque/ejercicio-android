package com.rubenorlandochoque.ejercicio.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.viewModels
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rubenorlandochoque.ejercicio.R
import com.rubenorlandochoque.ejercicio.adapters.ImageViewAdapter
import com.rubenorlandochoque.ejercicio.databinding.FragmentDeviceDetailBinding
import com.rubenorlandochoque.ejercicio.models.Device
import com.rubenorlandochoque.ejercicio.viewmodels.DevicesViewModel

class DeviceDetailFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentDeviceDetailBinding
    var imageViewAdapter: ImageViewAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDeviceDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this).asGif().load(R.drawable.searching).into(binding.searching)
        binding.searching.visibility = View.VISIBLE
        binding.deviceContainer.visibility = View.GONE
        val model: DevicesViewModel by viewModels()
        val id = requireArguments().getInt("deviceId")
        model.getDevice(id).observe(viewLifecycleOwner) { device ->
            showDevice(device)
            binding.searching.visibility = View.GONE
        }
    }

    private fun showDevice(device: Device) {
        imageViewAdapter = ImageViewAdapter(this, device.images.map { it.url })
        with(binding) {
            txtName.text = device.name
            imageViewer.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrollStateChanged(arg0: Int) {}
                override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
                override fun onPageSelected(currentPage: Int) {
                    txtCurrentImage.text = "${(currentPage + 1)} / ${device.images.size}"
                }
            })
            txtCurrentImage.text = "1 / ${device.images.size}"
            imageViewer.adapter = imageViewAdapter
            txtLegal.text = HtmlCompat.fromHtml(device.legal, HtmlCompat.FROM_HTML_MODE_LEGACY)
            deviceContainer.visibility = View.VISIBLE
        }
    }
}