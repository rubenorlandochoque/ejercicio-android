package com.rubenorlandochoque.ejercicio.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rubenorlandochoque.ejercicio.R
import com.rubenorlandochoque.ejercicio.adapters.DeviceAdapter
import com.rubenorlandochoque.ejercicio.databinding.FragmentDevicesBinding
import com.rubenorlandochoque.ejercicio.viewmodels.DevicesViewModel

class DevicesFragment : Fragment() {
    private lateinit var binding: FragmentDevicesBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDevicesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this).asGif().load(R.drawable.searching).into(binding.searching)
        binding.searching.visibility = View.VISIBLE
        val model: DevicesViewModel by viewModels()
        binding.devicesList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        model.getDevices().observe(viewLifecycleOwner) { devices ->
            val deviceAdapter = DeviceAdapter(this, devices)
            binding.devicesList.adapter = deviceAdapter
            binding.searching.visibility = View.GONE
        }
    }
}